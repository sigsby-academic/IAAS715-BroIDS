
# Abstract

Software development has embraced the principles and practices of agile and 
lean methodologies for some time.  In more recent years, this adoption of 
agile and lean has advanced to include operational teams and infrastructure 
through DevOps principles.  Information security is poised to benefit from 
these principles in several ways, particularly by adopting a DevOps style of 
delivering security services.  We will examine specific principles of agile 
and lean methodologies, common tools and practices used by development and 
operational teams, and how those principles and tools can be applied to 
automating the development and deployment of the Bro IDS (n.d.).  This will 
act as a test case to demonstrate that security services can be delivered 
following agile principles, particularly using automation, test-driven 
principles, infrastructure as code, and iterative and incremental processes.

_
Keywords: Bro IDS, Agile Security, Test-Driven Security, Software-Defined 
Security, Security as Code, Test-Driven Infrastructure, Software-Defined 
Infrastructure, Infrastructure as Code.
_

# Requirements

* git client (obviously)
* vagrant: https://www.vagrantup.com/
* virtualbox: https://www.virtualbox.org/
* ansible: https://www.ansible.com/
* bats: https://github.com/sstephenson/bats

