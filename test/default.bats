#!/usr/bin/env bats

rm -f .bats-setup .bats-teardown

setup() {
	date >> .bats-setup
}

teardown() {
	date >> .bats-teardown	
}

# helpers

clear_logs() {
	vagrant ssh sensor -c 'sudo /opt/bro/bin/broctl stop'
	vagrant ssh sensor -c 'sudo rm -rf /opt/bro/spool/bro/*.logs'
	vagrant ssh sensor -c 'sudo /opt/bro/bin/broctl deploy'
}


# tests

@test 'bro is running' {
	vagrant ssh sensor -c 'sudo /opt/bro/bin/broctl status'
}

@test 'bro detects a port scan' {
	clear_logs
	vagrant ssh client -c 'nmap www'
	vagrant ssh sensor -c 'sudo cat /opt/bro/logs/current/notice.log | bro-cut src dst note | grep -F "Scan::Port_Scan"'
}


